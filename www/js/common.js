
//Custom Functions for application

var SettingsData = {
	fcmchannel: ["none"],
	clockedin: false
}

var FCM = {
	type: "None",
	token: "None"
}

var PushNotificationData = {
	title: ["None"],
	message: ["No Notifications"]
}

var push = [];

//Handles all functions for Settings Storage and retreiving.
Settings = {
	load: function() {
		var retrievedSettings = window.localStorage.getItem("SettingsData");

		if (retrievedSettings == null) {} else {
			SettingsData = JSON.parse(retrievedSettings);
		}
		$('input[name=fcmchannel]').val(SettingsData.fcmchannel);
	},

	store: function() {
		var DataToStore = JSON.stringify(SettingsData);
		window.localStorage.setItem("SettingsData", DataToStore);
	},

	update: function() {
		SettingsData.fcmchannel = $('[name=fcmchannel]').val();
		$('input[name=fcmchannel]').val(SettingsData.fcmchannel);
		Settings.store();
	}
}


//Handles all the specific actions based on what the actual push message type is
PushHandler = {
	
	message: function(data){
		//data format:
		//data.additionalData.title = Title of Notification
		//data.additionalData.machine = Machine Announcing Messgae
		//data.additionalData.message = Actual Message from PLC
		$("#FCMMachine").html(data.additionalData.machine);
		$("#FCMMessage").html(data.additionalData.messgae);
		$('.ui.basic.modal#MessageFCM').modal('show');
		PushHandler.add(data.additionalData.title+": "+data.additionalData.machine, data.additionalData.message)
	},
	

	
	remove: function(messageID){
		if (PushNotificationData.title.length == 1) {
			PushNotificationData.title[0] = "None";
			PushNotificationData.message[0] = "No Notifications";
		} else {
			PushNotificationData.title.splice(messageID, 1);
			PushNotificationData.message.splice(messageID, 1);
		};
		PushHandler.store();
		PushHandler.display();
	},
	
	add: function(title, message){
		if (PushNotificationData.title[0] == "None") {
			PushNotificationData.title[0] = title;
			PushNotificationData.message[0] = message;
		} else {
			PushNotificationData.title.push(title);
			PushNotificationData.message.push(message);
		};
		PushHandler.store();
		PushHandler.display();
	},
	
	display: function(){
		$("#NotificationArea").empty();
		console.log(PushNotificationData.title.length);
		var index;
		for (index = 0; index < PushNotificationData.title.length; ++index) {
			$("#NotificationArea").prepend("<div class='ui message' onclick='PushHandler.remove("+index+")'><div class='header'>"+PushNotificationData.title[index]+"</div><p>"+PushNotificationData.message[index]+"</p></div>");
		}
		
	},
	
	load: function(){
		var retrievedPushNotificationData = window.localStorage.getItem("PushNotificationData");

		if (retrievedPushNotificationData == null) {} else {
			PushNotificationData = JSON.parse(retrievedPushNotificationData);
		}
		PushHandler.display();
	},
	
	store: function() {
		var DataToStore = JSON.stringify(PushNotificationData);
		window.localStorage.setItem("PushNotificationData", DataToStore);
		PushHandler.display();
	},
	
}	

//Handles the Punch in and out to receive messages		
Clock = {
	
	load: function(){
		console.log(SettingsData.clockedin);
		if(SettingsData.clockedin == true) {
			console.log("clocked in");
			Clock.punchin();
		}
	},
	
	punchin: function(){
		console.log("clocked in");
		SettingsData.clockedin = true;
		$("#ClockStatus").html("<i class='clock icon'></i>Clocked In");
		$("#ClockMessage").html("<p>Currently Clocked In and receiving Breakdown Notifications</p>");
		$("#ClockDiv").addClass("blue");
		$("#ClockDiv").removeClass("grey");	
		Settings.store();
		
		push.subscribe(SettingsData.fcmchannel, function () {
			console.log('topic sub success');
		}, function (e) {
			console.log('topic sub error:', e);
		});
		
	},
	
	punchout: function(){
		SettingsData.clockedin = false;
		$("#ClockStatus").html("<i class='clock outline icon'></i>Clocked Out");
		$("#ClockMessage").html("<p>Currently Clocked Out and not receiving Breakdown Notifications</p>");
		$("#ClockDiv").addClass("grey");
		$("#ClockDiv").removeClass("blue");
		Settings.store();
		
		push.unsubscribe(SettingsData.fcmchannel, function () {
			console.log('topic unsub success');
		}, function (e) {
			console.log('topic unsub error:', e);
		});
		
		
	}
	
}	

//Handles Push Notification Code
function PushNotifications(){
	push = PushNotification.init({
		android: {
				senderID: '564962855579' //Update this to match accoutn sender ID//
		},
		browser: {
			pushServiceURL: 'http://push.api.phonegap.com/v1/push'
		},
		ios: {
			alert: "true",
			badge: "true",
			sound: "true"
		},
		windows: {}
	});

	push.on('registration', function (data) {
		FCM.token = data.registrationId;
		FCM.type = data.registrationType;
		$("#PushStatus").html("Registered: " + FCM.type);
		$("#PushToken").html(FCM.token);
	});
	
	push.on('error', function (e) {
		alert("error:" + e);
	});

	push.on('notification', function (data) {
		PushHandler.message(data);
	});
}	


//Handles Application and Cordova Code
App = {
	init: function(){
		Settings.load();
		PushHandler.load();
		Clock.load();
		
	}
	
}

//Override Functions
$("#logForm").submit(function(event) {
	event.preventDefault();
});

//Stops enter key from doing shit when there is no enter key on a mobile deviceready
$(document).ready(function() {
	$(window).keydown(function(event) {
		if (event.keyCode == 13) {
			event.preventDefault();
			return false;
		}
	});
});


//Handles Navigation Function
$('.menu .item')
	.tab();

$('.menu .item')
	.on('click', function() {
		$('.ui.sidebar').sidebar('hide');
	});

//Handles Drop downs in forms
$('.selection.dropdown')
	.dropdown();

//Closes Popup Messages
$('.message .close')
	.on('click', function() {
		$(this)
			.closest('.message')
			.transition('fade');
	});

//UI Button Controllers - OnClick Functions

$("#MainMenu").click(function() {
	$('.ui.sidebar').sidebar('toggle');
});

$("#SettingsRefresh").click(function() {
	event.preventDefault();
	Settings.update();
});


$("#PushNotifications").click(function() {
	event.preventDefault();
	$('.ui.basic.modal#FCMStatus').modal('show');
});


$( "#ClockPress" ).click(function() {
	if (SettingsData.fcmchannel === "none") {
		$('#channelerror').toast({
			showProgress: 'bottom',
			classProgress: 'red',
			position: 'top center',
			displayTime: 2000
		});
		
	} else if (SettingsData.clockedin == false) {
		Clock.punchin();
	} else {
		Clock.punchout();
	}
});

// Form Validation Code

function onBackButton() {
	wikiframe.history.back();
}

function onWindowLoad() {

}

function onDeviceReady(){
	PushNotifications();
}

//Document and Window Event Handler Code


document.addEventListener("backbutton", onBackButton, false);
window.addEventListener("load", onWindowLoad, false);
document.addEventListener('DOMContentLoaded', App.init, false);
document.addEventListener('deviceready', onDeviceReady, false);



